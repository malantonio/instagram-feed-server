// use this as a temporary endpoint to retrieve a login token
const express = require('express')
const app = express()

app.get('/', (req, res) => {
  let html = `
  <!doctype html>
  <html>
    <body>
      hey yr token: <strong class="token"></strong>
      <script>
      if (document.location.hash) {
        document.querySelector('.token').textContent = document.location.hash
      }
      </script>
    </body>
  </html>`

  res.send(html)
})

app.listen(process.env.PORT || 8080)