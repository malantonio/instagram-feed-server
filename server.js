// builds a super basic endpoint to grab the latest n entries 
// from an instagram feed and serve the cleaned json responses
const https = require('https')
const express = require('express')
const config = require('./config.json')

const user = config.username
const token = config.token
const limit = config.limit || 3
const URL = `https://api.instagram.com/v1/users/self/media/recent?access_token=${token}`

const CACHE_TTL = 1000 * 60 * 60 // one hour

const app = express()
let cache = null

app.get('/', (req, res) => {
  res.set('Access-Control-Allow-Origin', 'http://localhost:4000')
  if (req.fresh && cache && cache.expires > Date.now()) {
    res.set('Cache-Control', 'max-age=' + cache.expires)
    return res.json(cache.data)
  }

  getData((err, items) => {
    if (err) {
      throw err
      return res.status(500).json({error: err.message})
    }

    cache = {
      expires: Date.now() + CACHE_TTL,
      data: cleanData(items.data),
    }

    res.set('Cache-Control', 'max-age=' + cache.expires)
    return res.json({iam: 'frehs', data: cache.data})
  })
})

app.listen(process.env.PORT || config.port || 8080)

function getData (callback) {
  https.get(URL, res => {
    res.setEncoding('utf8')

    let body = ''

    res.on('data', d => body += d)
    res.on('end', () => {
      try {
        return callback(null, JSON.parse(body))
      } catch (e) {
        return callback(e)
      }
    })
  })
}

function cleanData (data) {
  let count = 0
  return data.filter(item => {
    if (count >= limit) {
      return false
    }

    if (item.type !== 'image') {
      return false
    }

    count += 1
    return true
  }).map(item => ({
    image: item.images.low_resolution,
    url: item.link,
    created_at: item.created_time,
    caption: item.caption ? item.caption.text : null,
    type: item.type,
  }))
}